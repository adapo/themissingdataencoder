from datetime import datetime
import os
import sys
import time
import math
import numpy as np
import tensorflow as tf
import vgg16 as vgg16
from PIL import Image

from mde_archis_v2 import generator,discriminator,maskrec
from mde_imagereader_v2 import ImageReader

class Model(object):

    def __init__(self, sess, conf):
        self.sess = sess
        self.conf = conf

    # train
    def train(self):
        
        #random seed
        tf.set_random_seed(self.conf.random_seed)
        
        # Create queue coordinator.
        self.coord = tf.train.Coordinator()
        
        # Input size
        input_size = (self.conf.input_height, self.conf.input_width)
        
        self.surface = tf.placeholder(dtype=tf.float32, shape=())
        
        # Load reader
        with tf.name_scope("create_inputs"):
            reader = ImageReader(
                self.conf.data_dir,
                self.conf.data_list,
                self.conf.landmark_list_celeba,
                self.conf.bbox_list_celeba,
                input_size,
                True,
                self.conf.surface,
                self.coord)
            self.image_batch, self.mask_in, mask_out, mask_in_rd, mask_out_rd = reader.dequeue(self.conf.batch_size)
        
        input_batch = self.image_batch * self.mask_in
        
        # Create networks
        gen, g_logits, fc1 = generator(input_batch,istraining=True)

        disc_real, d_logits_r = discriminator(self.image_batch, istraining=True)
        disc_fake, d_logits_f = discriminator(gen, istraining=True, reuse=True)
        
        self.rec_map, rec_map_raw = maskrec(gen, istraining=True)
        
        # Variables that load from pre-trained model.
        restore_var = [v for v in tf.global_variables() if 'mask' not in v.name]
        # Trainable Variables
        all_trainable = tf.trainable_variables()
        # Fine-tune part
        generator_trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='Generator')
        # Decoder part
        discriminator_trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='Discriminator')
        discriminator2_trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='Maskrec')
        
        # Sanity check
        print(len(all_trainable))
        print(len(generator_trainable))
        print(len(discriminator_trainable))
        print(len(discriminator2_trainable))
        
        assert(len(all_trainable) == len(generator_trainable) + len(discriminator_trainable) + len(discriminator2_trainable))

        disc_label_real=np.ones([self.conf.batch_size,1],np.float32)
        disc_label_fake=np.zeros([self.conf.batch_size,1],np.float32)

        self.curr_step = tf.placeholder(dtype=tf.float32, shape=())
        self.update_disc = tf.placeholder(dtype=tf.float32, shape=())

        #perceptual loss
        vgg = vgg16.Vgg16()
        vgg1 = vgg16.Vgg16()
        
        genrsz = tf.image.resize_images(gen, [224,224])
        gtrsz = tf.image.resize_images(self.image_batch, [224,224])
        
        vgg.build((genrsz))
        vgg1.build((gtrsz))
        
        lconv1=1.
        lconv2=0.5
        lconv3=0.25
        lconv4=0.125
        lconv5=0.0625
        lfc1=0.
        lfc2=0.
        lfc3=0.
        lall=lconv1+lconv2+lconv3+lconv4+lconv5+lfc1+lfc2+lfc3
        lconv1=lconv1/lall
        lconv2=lconv2/lall
        lconv3=lconv3/lall
        lconv4=lconv4/lall
        lconv5=lconv5/lall
        lfc1=lfc1/lall
        lfc2=lfc2/lall
        lfc3=lfc3/lall
        
        vggloss_conv1 = tf.reduce_mean(tf.pow(vgg.conv1_2-vgg1.conv1_2,2))
        vggloss_conv2 = tf.reduce_mean(tf.pow(vgg.conv2_2-vgg1.conv2_2,2))
        vggloss_conv3 = tf.reduce_mean(tf.pow(vgg.conv3_3-vgg1.conv3_3,2))
        vggloss_conv4 = tf.reduce_mean(tf.pow(vgg.conv4_3-vgg1.conv4_3,2))
        vggloss_conv5 = tf.reduce_mean(tf.pow(vgg.conv5_3-vgg1.conv5_3,2))
        vggloss_fc6 = tf.reduce_mean(tf.pow(vgg.fc6-vgg1.fc6,2))
        vggloss_fc7 = tf.reduce_mean(tf.pow(vgg.fc7-vgg1.fc7,2))
        vggloss_fc8 = tf.reduce_mean(tf.pow(vgg.fc8-vgg1.fc8,2))
        vgg_loss=lconv1*vggloss_conv1+lconv2*vggloss_conv2+lconv3*vggloss_conv3+lconv4*vggloss_conv4+lconv5*vggloss_conv5+lfc1*vggloss_fc6+lfc2*vggloss_fc7+lfc3*vggloss_fc8
        
        loverlap = 1.
        lnorm = 0.
        lvgg = 0.00002
        lrec=0.995
        lbox=10.0
        ladv=1.-lrec
        gen_loss_tot = tf.reduce_mean(tf.pow(gen*mask_out - self.image_batch*mask_out, 2))
        gen_loss_overlap = tf.reduce_mean(tf.pow(gen*self.mask_in - self.image_batch*self.mask_in, 2))
        gen_loss_rec = lnorm*gen_loss_tot + loverlap*gen_loss_overlap + lvgg*vgg_loss
        
        self.disc_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_r, labels=disc_label_real))
        self.disc_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_f, labels=disc_label_fake))
        gen_loss_adv = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_f, labels=tf.ones_like(disc_fake)))
        
        #HnS loss
        self.disc_box_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=rec_map_raw, labels=self.mask_in))
        self.gen_loss_box = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=rec_map_raw, labels=mask_in_rd))
        gen_loss_box = lbox*self.gen_loss_box
        
        #combo losses
        self.gen_loss = lrec * gen_loss_rec + ladv*(gen_loss_adv + gen_loss_box)
        self.disc_loss = ladv*(self.disc_loss_real+self.disc_loss_fake)
        self.disc_loss = self.disc_loss + ladv * lbox * self.disc_box_loss

        # Define optimizers
        # 'poly' learning rate
        base_lr = tf.constant(self.conf.learning_rate)
        learning_rate = tf.scalar_mul(base_lr, tf.pow((1 - self.curr_step / self.conf.num_steps), self.conf.power))
        
        opt_generator = tf.train.AdamOptimizer(10.*learning_rate,self.conf.momentum)
        opt_discriminator = tf.train.AdamOptimizer(learning_rate,self.conf.momentum)#apply different lr for discriminator (possibly reduced one)
        
        grads_generator = tf.gradients(self.gen_loss, generator_trainable)
        grads_discriminator = tf.gradients(self.disc_loss, discriminator_trainable+discriminator2_trainable)
        
        train_op_gen = opt_generator.apply_gradients(zip(grads_generator, generator_trainable))
        train_op_disc = opt_discriminator.apply_gradients(zip(grads_discriminator, discriminator_trainable+discriminator2_trainable))
            
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_op = tf.group(train_op_gen, train_op_disc)

        # Saver for storing checkpoints of the model
        self.saver = tf.train.Saver(var_list=tf.global_variables(), max_to_keep=0)

        # Loader for loading the pre-trained model
        self.loader = tf.train.Saver(var_list=restore_var)

        # Processed predictions: for visualisation
        self.pred = gen
        
        self.sess.run(tf.global_variables_initializer())

        # Start queue threads
        threads = tf.train.start_queue_runners(coord=self.coord, sess=self.sess)

        # Train loop
        for step in range(self.conf.num_steps+1):
            start_time = time.time()
            
            feed_dict = { self.curr_step : step}

            if step > 0 and step % self.conf.save_interval == 0:
                gen_loss_value, disc_loss_real, disc_loss_fake, images,preds,maskin,disc_recmap,_gen_loss_box, _ = self.sess.run(
                    [self.gen_loss,
                     self.disc_loss_real,
                     self.disc_loss_fake,
                    self.image_batch,
                    self.pred,
                    self.mask_in,
                    self.rec_map,
                    gen_loss_box,
                    self.train_op],
                    feed_dict=feed_dict)
                self.save(self.saver, step)
            else:
                gen_loss_value, disc_loss_real, disc_loss_fake,images,preds,maskin,disc_recmap,_gen_loss_box, _ = self.sess.run(
                    [self.gen_loss,
                    self.disc_loss_real,
                    self.disc_loss_fake,
                    self.image_batch,
                    self.pred,
                    self.mask_in,
                    self.rec_map,
                    gen_loss_box,
                    self.train_op],
                    feed_dict=feed_dict)

            duration = time.time() - start_time
            print('step {:d} \t gen = {:.3f}, disc_real = {:.6f}, disc_fake = {:.6f}, box = {:.6f}  ({:.3f} sec/step)'.format(step, gen_loss_value, disc_loss_real, disc_loss_fake, _gen_loss_box, duration))

            if step == 100 or step % 500 == 0:

               p0 = np.reshape(preds[0],newshape=(self.conf.input_height, self.conf.input_width, 3))
               p1 = np.reshape(disc_recmap[0],newshape=(self.conf.input_height, self.conf.input_width, 3))
               p2 = np.reshape(maskin[0],newshape=(self.conf.input_height, self.conf.input_width, 3))
#               
               directory = self.conf.visus_train_dir
               if not os.path.exists(directory):
                   os.makedirs(directory)
                   
               filename = '%s/%d.png' % (directory, step)
               filename_mask = '%s/%d_mask.png' % (directory, step)
               filename_mask_gt = '%s/%d_mask_gt.png' % (directory, step)
               
               im = Image.fromarray(np.uint8(p0*255))
               im_mask = Image.fromarray(np.uint8(p1*255))
               im_mask_gt = Image.fromarray(np.uint8(p2*255))
               
               im.save(filename)
               im_mask.save(filename_mask)
               im_mask_gt.save(filename_mask_gt)

        # finish
        self.coord.request_stop()
        self.coord.join(threads)

    # prediction
    def predict(self):
        #set batch size to 1
        self.conf.batch_size=1
        
        #random seed
        tf.set_random_seed(self.conf.random_seed)
        
        # Create queue coordinator.
        self.coord = tf.train.Coordinator()

        input_size = (self.conf.input_height, self.conf.input_width)
        
        # Load reader
        with tf.name_scope("create_inputs"):
            reader = ImageReader(
                self.conf.data_dir,
                self.conf.data_list,
                self.conf.landmark_list_celeba,
                self.conf.bbox_list_celeba,
                input_size,
                False,
                self.conf.surface,
                self.coord)
            self.image_batch, self.mask_in, mask_out, mask_in_rd, mask_out_rd = reader.dequeue(self.conf.batch_size)

        # Create network
        input_batch=self.image_batch * self.mask_in
        gen, g_logits, fc1 = generator(input_batch,istraining=False)

        # Predictionsz
        self.pred = 255*gen
        self.masks = 255*self.mask_in
        self.images= 255*self.image_batch

        # Create directory
        if not os.path.exists(self.conf.out_dir):
            os.makedirs(self.conf.out_dir)

        # Loader for loading the checkpoint
        self.loader = tf.train.Saver(var_list=tf.all_variables())

        self.sess.run(tf.global_variables_initializer())
        self.sess.run(tf.local_variables_initializer())

        # load checkpoint
        checkpointfile = self.conf.modeldir+ '/model.ckpt-' + str(self.conf.valid_step)
        self.load(self.loader, checkpointfile)

        # Start queue threads.
        threads = tf.train.start_queue_runners(coord=self.coord, sess=self.sess)

        for step in range(self.conf.test_num_steps):
            
            images,preds,masks = self.sess.run([self.images,self.pred,self.masks])

            img_name = str(step)
            
            rgbArray = np.zeros((96,96,3), 'float32')
            rgbArray[:,:, 0] = preds[0,:,:,0]
            rgbArray[:,:, 1] = preds[0,:,:,1]
            rgbArray[:,:, 2] = preds[0,:,:,2]
            im = Image.fromarray(rgbArray.astype('uint8'))
            filename = '/%s_pred.png' % (img_name)
            im.save(self.conf.out_dir + filename)
            
            rgbArray_r = np.zeros((96,96,3), 'float32')
            rgbArray_r[:,:, 0] = masks[0,:,:,0]
            rgbArray_r[:,:, 1] = masks[0,:,:,1]
            rgbArray_r[:,:, 2] = masks[0,:,:,2]
            im_r = Image.fromarray(rgbArray_r.astype('uint8'))
            filename_r = '/%s_mask.png' % (img_name)
            im_r.save(self.conf.out_dir + filename_r)
            
            rgbArray_lb = np.zeros((96,96,3), 'float32')
            rgbArray_lb[:,:, 0] = images[0,:,:,0]
            rgbArray_lb[:,:, 1] = images[0,:,:,1]
            rgbArray_lb[:,:, 2] = images[0,:,:,2]
            lb = Image.fromarray(rgbArray_lb.astype('uint8'))
            filename_lb = '/%s_lbl.png' % (img_name)
            lb.save(self.conf.out_dir + filename_lb)

            print('step {:d}'.format(step))

        print('The output files has been saved to {}'.format(self.conf.out_dir))

        # finish
        self.coord.request_stop()
        self.coord.join(threads)
        
    def save(self, saver, step):
        '''
        Save weights.
        '''
        model_name = 'model.ckpt'
        checkpoint_path = os.path.join(self.conf.modeldir, model_name)
        if not os.path.exists(self.conf.modeldir):
            os.makedirs(self.conf.modeldir)
        saver.save(self.sess, checkpoint_path, global_step=step)
        print('The checkpoint has been created.')

    def load(self, saver, filename):
        '''
        Load trained weights.
        ''' 
        saver.restore(self.sess, filename)
        print("Restored model parameters from {}".format(filename))