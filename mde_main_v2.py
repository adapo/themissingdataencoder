import argparse
import os
import tensorflow as tf
from mde_models_v2 import Model

def configure():
	flags = tf.app.flags

	# training hyperparameters
	flags.DEFINE_integer('num_steps', 200000, 'number of train iterations')
	flags.DEFINE_integer('save_interval', 200000, 'number of iterations for saving')
	flags.DEFINE_integer('random_seed', 1234, 'random seed')
	flags.DEFINE_float('learning_rate', 2e-5, 'initial learning rate')
	flags.DEFINE_float('power', 0.9, 'hyperparameter for poly learning rate annealing')
	flags.DEFINE_float('momentum', 0.9, 'hyperparameter for momentum')
	flags.DEFINE_float('surface', 0.2, 'hyperparameter S for non-dropped surface')
	flags.DEFINE_integer('batch_size', 16, 'training batch size')
	flags.DEFINE_integer('input_height', 96, 'input image height')
	flags.DEFINE_integer('input_width', 96, 'input image width')
	
	# prediction
	flags.DEFINE_integer('valid_step', 200000, '= number of testing samples')
	flags.DEFINE_integer('test_num_steps', 1200, '= number of testing samples')
	
	# paths for CelebA database
	flags.DEFINE_string('data_dir', '../datakalab.image-database/', 'root data directory')
	flags.DEFINE_string('data_list', '../datakalab.image-database/face_alignment/data_list/celeba_train_noalign.txt', 'training data list filename')
	flags.DEFINE_string('test_data_list', '../datakalab.image-database/face_alignment/data_list/celeba_test.txt', 'testing data list filename')
	flags.DEFINE_string('landmark_list_celeba', '../datakalab.image-database/face_alignment/data_list/list_landmarks_celeba.txt', 'list of landmarks (5-xy)')
	flags.DEFINE_string('bbox_list_celeba', '../datakalab.image-database/face_alignment/data_list/list_bbox_celeba.txt', 'list of bbox localizations')
	flags.DEFINE_string('out_dir', 'visus_mde_test', 'output test visus directory')
	
	# output paths
	flags.DEFINE_string('modeldir', 'model_mde', 'model directory')
	flags.DEFINE_string('visus_train_dir', './visus_mde', 'output train visus directory')
	
	flags.FLAGS.__dict__['__parsed'] = False
	return flags.FLAGS

def main(_):
	parser = argparse.ArgumentParser()
	parser.add_argument('--option', dest='option', type=str, default='train',
		help='actions: train or predict')
	args = parser.parse_args()

	if args.option not in ['train', 'predict']:
		print('invalid option: ', args.option)
		print("Please input an option: train, predict")
	else:
	    
		config = tf.ConfigProto()
		config.gpu_options.allow_growth = True
		sess = tf.Session(config=config)
		
		model = Model(sess, configure())
		getattr(model, args.option)()

if __name__ == '__main__':
    tf.app.run()