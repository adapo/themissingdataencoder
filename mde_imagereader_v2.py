import os

import numpy as np
import random as rd
import tensorflow as tf
import matplotlib as plt

def read_labeled_image_list(data_dir, data_list):
    f = open(data_list, 'r')
    images = []
    masks = []
    for line in f:
        try:
            image, mask = line.strip("\n").split(' ')
        except ValueError: # Adhoc for test.
            image = mask = line.strip("\n")
        images.append(data_dir + image)
        masks.append(data_dir + mask)
    return images

def random_mask_1c(surface, window_size):
    '''
    generate a random mask for a single channel
    arguments: 
    - surface (hyperparameter S of non-dropped surface, 0-1 float)
    - window size (image size)
    outputs:
    - mask_in: a single channel mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another single channel mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    delta=5.
    
    h0, w0 = window_size
    
    hc=tf.constant(h0,shape=[1])
    wc=tf.constant(w0,shape=[1])
    cc=tf.constant(1,shape=[1])
    
    h = tf.random_uniform([1],minval=h0*surface,maxval=h0,dtype=tf.float32)
    w = surface*h0*w0/h
        
    posy = tf.cast(tf.random_uniform([1],minval=0.,maxval=h0-h,dtype=tf.float32),tf.int32)
    posx = tf.cast(tf.random_uniform([1],minval=0.,maxval=w0-w,dtype=tf.float32),tf.int32)
     
    h2 = tf.random_uniform([1],minval=h0*surface,maxval=h0,dtype=tf.float32)
    w2 = surface*h0*w0/h2
    posy2 = tf.cast(tf.random_uniform([1],minval=0.,maxval=h0-h2,dtype=tf.float32),tf.int32)
    posx2 = tf.cast(tf.random_uniform([1],minval=0.,maxval=w0-w2,dtype=tf.float32),tf.int32)
    h2 = tf.cast(h2,tf.int32)
    w2 = tf.cast(w2,tf.int32)
    
    h = tf.cast(h,tf.int32)
    w = tf.cast(w,tf.int32)
    
    padsize0_1 = tf.expand_dims(tf.concat([posy,hc-h-posy],axis=0),axis=1)
    padsize0_2 = tf.expand_dims(tf.concat([posx,wc-w-posx],axis=0),axis=1)
    padsize0_3 = tf.zeros([2,1],dtype=tf.int32)
    padsize0 = tf.transpose(tf.concat([padsize0_1, padsize0_2, padsize0_3],axis=1))
        
    padsize2_1 = tf.expand_dims(tf.concat([posy2,hc-h2-posy2],axis=0),axis=1)
    padsize2_2 = tf.expand_dims(tf.concat([posx2,wc-w2-posx2],axis=0),axis=1)
    padsize2_3 = tf.zeros([2,1],dtype=tf.int32)
    padsize2 = tf.transpose(tf.concat([padsize2_1, padsize2_2, padsize2_3],axis=1))
        
    size0 = tf.concat([h,w,cc],axis=0)
    rawmask=tf.ones(size0,dtype=tf.float32)
    mask_in=tf.pad(rawmask, padsize0, "CONSTANT")
    mask_in.set_shape((h0, w0, 1))   
    mask_out=tf.ones([h0,w0,1],dtype=tf.float32)-mask_in
    
    size2 = tf.concat([h2,w2,cc],axis=0)
    rawmask2=tf.ones(size2,dtype=tf.float32)
    mask_in_2=tf.pad(rawmask2, padsize2, "CONSTANT")
    mask_in_2.set_shape((h0, w0, 1))   
    mask_out_2=tf.ones([h0,w0,1],dtype=tf.float32)-mask_in_2
    
    return mask_in, mask_out, mask_in_2, mask_out_2

def random_mask_3c(surface, window_size):
    '''
    generate a random mask for 3 channels
    arguments: 
    - surface (hyperparameter S of non-dropped surface, 0-1 float)
    - window size (image size)
    outputs:
    - mask_in: a 3-channels mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another 3-channels mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    
    mask_in_c1, mask_out_c1, mask_in_2_c1, mask_out_2_c1 = random_mask_1c(surface, window_size)
    mask_in_c2, mask_out_c2, mask_in_2_c2, mask_out_2_c2 = random_mask_1c(surface, window_size)
    mask_in_c3, mask_out_c3, mask_in_2_c3, mask_out_2_c3 = random_mask_1c(surface, window_size)
    
    mask_in = tf.concat([mask_in_c1,mask_in_c2,mask_in_c3],axis=2)
    mask_out = tf.concat([mask_out_c1,mask_out_c2,mask_out_c3],axis=2)
    mask_in_2 = tf.concat([mask_in_2_c1,mask_in_2_c2,mask_in_2_c3],axis=2)
    mask_out_2 = tf.concat([mask_out_2_c1,mask_out_2_c2,mask_out_2_c3],axis=2)
    
    return mask_in, mask_out, mask_in_2, mask_out_2

def random_mask_inpainting(surface, window_size):
    '''
    generate an inpaiting mask for 3 channels
    only the middle half of the image remains
    arguments: 
    - surface (hyperparameter S of non-dropped surface, 0-1 float)
    - window size (image size)
    outputs:
    - mask_in: a 3-channels mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another 3-channels mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    
    h0, w0 = window_size
    
    h = tf.constant(surface*h0,shape=[1])
    w = tf.constant(surface*w0,shape=[1])
        
    posy = tf.cast((w0-w)/2,tf.int32)
    posx = tf.cast((h0-h)/2,tf.int32)
    h = tf.cast(h,tf.int32)
    w = tf.cast(w,tf.int32)
     
    hc=tf.constant(h0,shape=[1])
    wc=tf.constant(w0,shape=[1])
    cc=tf.constant(3,shape=[1])
    
    padsize0_1 = tf.expand_dims(tf.concat([posy,hc-h-posy],axis=0),axis=1)
    padsize0_2 = tf.expand_dims(tf.concat([posx,wc-w-posx],axis=0),axis=1)
    padsize0_3 = tf.zeros([2,1],dtype=tf.int32)
    padsize0 = tf.transpose(tf.concat([padsize0_1, padsize0_2, padsize0_3],axis=1))
        
    size0 = tf.concat([h,w,cc],axis=0)
    rawmask=tf.ones(size0,dtype=tf.float32)
    mask_out=tf.pad(rawmask, padsize0, "CONSTANT")
    mask_out.set_shape((h0, w0, 3))   
    mask_in=tf.ones([h0,w0,3],dtype=tf.float32)-mask_out
    
    return mask_in, mask_out, mask_in, mask_out

def random_mask_outpainting(surface, window_size):
    '''
    generate an outpaiting mask for 3 channels
    only the middle half of the image is dropped
    arguments: 
    - surface (hyperparameter S of non-dropped surface, 0-1 float)
    - window size (image size)
    outputs:
    - mask_in: a 3-channels mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another 3-channels mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    
    h0, w0 = window_size
    
    h = tf.constant(surface*h0,shape=[1])
    w = tf.constant(surface*w0,shape=[1])
        
    posy = tf.cast((w0-w)/2,tf.int32)
    posx = tf.cast((h0-h)/2,tf.int32)
    h = tf.cast(h,tf.int32)
    w = tf.cast(w,tf.int32)
     
    hc=tf.constant(h0,shape=[1])
    wc=tf.constant(w0,shape=[1])
    cc=tf.constant(3,shape=[1])
    
    padsize0_1 = tf.expand_dims(tf.concat([posy,hc-h-posy],axis=0),axis=1)
    padsize0_2 = tf.expand_dims(tf.concat([posx,wc-w-posx],axis=0),axis=1)
    padsize0_3 = tf.zeros([2,1],dtype=tf.int32)
    padsize0 = tf.transpose(tf.concat([padsize0_1, padsize0_2, padsize0_3],axis=1))
        
    size0 = tf.concat([h,w,cc],axis=0)
    rawmask=tf.ones(size0,dtype=tf.float32)
    mask_in=tf.pad(rawmask, padsize0, "CONSTANT")
    mask_in.set_shape((h0, w0, 3))   
    mask_out=tf.ones([h0,w0,3],dtype=tf.float32)-mask_in
    
    return mask_in, mask_out, mask_in, mask_out

def random_mask_col_1(window_size):
    '''
    generate a colorization mask for 3 channels
    only one channel remains
    arguments: 
    - window size (image size)
    outputs:
    - mask_in: a 3-channels mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another 3-channels mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    
    h0, w0 = window_size
    
    nc0 = tf.cast(tf.random_uniform([1],minval=0,maxval=2,dtype=tf.int32),tf.float32)
     
    h2 = tf.random_uniform([1],minval=h0*0.5,maxval=h0,dtype=tf.float32)
    w2 = 0.5*h0*w0/h2
    posy2 = tf.cast(tf.random_uniform([1],minval=0.,maxval=h0-h2,dtype=tf.float32),tf.int32)
    posx2 = tf.cast(tf.random_uniform([1],minval=0.,maxval=w0-w2,dtype=tf.float32),tf.int32)
    h2 = tf.cast(h2,tf.int32)
    w2 = tf.cast(w2,tf.int32)
     
    mask_ones = tf.ones([h0,w0,1])
    mask_zeros = tf.zeros([h0,w0,1])
    
    perm_0=tf.concat([mask_zeros, mask_ones, mask_ones], axis=2)
    perm_1=tf.concat([mask_ones, mask_zeros, mask_ones], axis=2)
    perm_2=tf.concat([mask_ones, mask_ones, mask_zeros], axis=2)
    
    mask_in=((nc0-1)*(nc0-2.)/2.)*perm_0-((nc0-0)*(nc0-2.))*perm_1+((nc0-0)*(nc0-1.)/2.)*perm_2
    
    mask_in.set_shape((h0, w0, 3))   
    mask_out=tf.ones([h0,w0,3],dtype=tf.float32)-mask_in
    
    return mask_in, mask_out, mask_in, mask_out

def random_mask_col_2(window_size):
    '''
    generate a colorization mask for 3 channels
    only one channel is dropped
    arguments: 
    - window size (image size)
    outputs:
    - mask_in: a 3-channels mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another 3-channels mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    
    h0, w0 = window_size
    
    nc0 = tf.cast(tf.random_uniform([1],minval=0,maxval=2,dtype=tf.int32),tf.float32)
     
    h2 = tf.random_uniform([1],minval=h0*0.5,maxval=h0,dtype=tf.float32)
    w2 = 0.5*h0*w0/h2
    posy2 = tf.cast(tf.random_uniform([1],minval=0.,maxval=h0-h2,dtype=tf.float32),tf.int32)
    posx2 = tf.cast(tf.random_uniform([1],minval=0.,maxval=w0-w2,dtype=tf.float32),tf.int32)
    h2 = tf.cast(h2,tf.int32)
    w2 = tf.cast(w2,tf.int32)
     
    mask_ones = tf.ones([h0,w0,1])
    mask_zeros = tf.zeros([h0,w0,1])
    
    perm_0=tf.concat([mask_zeros, mask_ones, mask_ones], axis=2)
    perm_1=tf.concat([mask_ones, mask_zeros, mask_ones], axis=2)
    perm_2=tf.concat([mask_ones, mask_ones, mask_zeros], axis=2)
    
    mask_out=((nc0-1)*(nc0-2.)/2.)*perm_0-((nc0-0)*(nc0-2.))*perm_1+((nc0-0)*(nc0-1.)/2.)*perm_2
    
    mask_out.set_shape((h0, w0, 3))   
    mask_in=tf.ones([h0,w0,3],dtype=tf.float32)-mask_out
    
    return mask_in, mask_out, mask_in, mask_out

def read_images_from_disk(input_queue, input_size, surface, img_landmarks_data, img_bbox_data):
    '''
    read images from input queue
    arguments: 
    - input_queue (self-explanatory)
    - input_size (size of the crops)
    - surface: hyperparameter of non-dropped regions
    - img_landmarks_data: landmark locations (xy) for each image, used to align
    the images
    - img_bbox_data: bounding box location for each image, used to crop
    the images
    outputs:
    - crop: the actual face crop
    - mask_in: a 3-channels mask (non-masked regions)
    - mask_out: complementary of mask_in
    - mask_in_2: another 3-channels mask (non-masked regions)
    - mask_out_2: complementary of mask_in_2
    '''
    
    img_contents = tf.read_file(input_queue[0])
    
    #this depends on your path length! the key is the image number.
    #e.g. myfolder/000024.jpg -> 000024 -> 23
    key = tf.substr(input_queue[0],39,6)
    key = tf.string_to_number(key, out_type=tf.int32)-1

    bbox=tf.convert_to_tensor(img_bbox_data[key], dtype=tf.int32)
    
    landmarks = tf.convert_to_tensor(img_landmarks_data[key], dtype=tf.int32)
    
    landmarks=tf.to_float(landmarks)
    landmarks=tf.reshape(landmarks,[5,2])

    img = tf.image.decode_image(img_contents, channels=3)
    img=tf.cast(img,dtype=float)/255.
    
    #rotate the image and bbox to ensure zero roll
    thetagt=tf.math.atan2(landmarks[0,1]-landmarks[1,1],landmarks[0,0]-landmarks[1,0])+3.14159265359
    
    yydim=input_size[0]
    xxdim=input_size[1]
    
    margincst=tf.random_uniform([1], minval=0.5, maxval=0.5, dtype=tf.float32, seed=None)
        
    #apply crop based on landmark coordinates
    mx=bbox[0:2]
    Dm=bbox[2:4]
    mm=tf.multiply(tf.cast(Dm,dtype=tf.float32), margincst)
    Dm=tf.cast(Dm,dtype=tf.int32)
    mm=tf.cast(mm,dtype=tf.int32)
    marginy=mm[1]
    marginx=mm[0]
    mx=tf.cast(mx,dtype=tf.int32)
    
    padsize0_1=tf.transpose(tf.expand_dims(tf.stack([marginy, marginy]), axis=1))
    padsize0_2=tf.transpose(tf.expand_dims(tf.stack([marginx, marginx]), axis=1))
    padsize0_3=tf.zeros([1,2],dtype=tf.int32)
    padsize0 = tf.concat([padsize0_1, padsize0_2, padsize0_3],axis=0)
    
    shape=tf.shape(img)
    img=tf.pad(img, padsize0, "REFLECT")
    
    topleftx=mx[0]
    toplefty=mx[1]
    width=Dm[0]+2*marginx
    height=Dm[1]+2*marginy
    
    crop=tf.image.crop_to_bounding_box(img,toplefty,topleftx,height,width)
    crop=tf.contrib.image.rotate(crop,thetagt,interpolation='BILINEAR')
    crop=tf.image.crop_to_bounding_box(crop,marginy,marginx,Dm[1],Dm[0])
    shape=tf.shape(crop)
    
    ydim=shape[0]
    xdim=shape[1]
        
    rszx=tf.cast(xxdim,dtype=tf.float32)/tf.cast(xdim,dtype=tf.float32)
    rszy=tf.cast(yydim,dtype=tf.float32)/tf.cast(ydim,dtype=tf.float32)
    rsz=tf.stack([rszx, rszy])
        
    crop=tf.image.resize_images(crop, input_size)
    crop.set_shape((yydim, xxdim, 3))
    
    #generate random mask
    #for REC (random_mask_3c) the two firsts masks are different from the two
    #first ones. For inpainting/outpainting/colorization this is not the case,
    #as the process is deterministic HnS loss shall not be used.
    #mask_in, mask_out, mask_in_2, mask_out_2  = random_mask_inpainting(surface, input_size)
    #mask_in, mask_out, mask_in_2, mask_out_2  = random_mask_outpainting(surface, input_size)
    mask_in, mask_out, mask_in_2, mask_out_2  = random_mask_3c(surface, input_size)

    return crop, mask_in, mask_out, mask_in_2, mask_out_2

class ImageReader(object):
    def __init__(self, data_dir, data_list,landmarks_path,bbox_path,input_size,is_training,surface,coord):
        self.data_dir = data_dir
        self.data_list = data_list
        self.input_size = input_size
        self.surface = surface
        self.coord = coord
            
        self.image_list = read_labeled_image_list(self.data_dir, self.data_list) 
        self.images = tf.convert_to_tensor(self.image_list, dtype=tf.string)
        
        with tf.gfile.Open(bbox_path) as f:
            bbox_raw = f.read()
            
        with tf.gfile.Open(landmarks_path) as f:
            landmarks_raw = f.read()
        
        def process_bbox(raw_data):
            bbox = []
            lines = raw_data.split("\n")
            for line in lines[2:-1]:
                values = line.strip().split()
                bbox_values = [int(v) for v in values[1:]]
                bbox.append(bbox_values)
            return bbox

        def process_landmarks(raw_data):
            landmarks = []
            lines = raw_data.split("\n")
            for line in lines[2:-1]:
                values = line.strip().split()
                landmark_values = [int(v) for v in values[1:]]
                landmarks.append(landmark_values)
            return landmarks

        img_bbox = process_bbox(bbox_raw)
        img_landmarks = process_landmarks(landmarks_raw)
        
        img_bbox_data = tf.convert_to_tensor(img_bbox)
        img_landmarks_data = tf.convert_to_tensor(img_landmarks)
        
        self.image_list = read_labeled_image_list(self.data_dir, self.data_list) 
        self.images = tf.convert_to_tensor(self.image_list, dtype=tf.string)
        self.queue = tf.train.slice_input_producer([self.images], shuffle=is_training)
        
        self.image, self.mask_in, self.mask_out, self.mask_in_2, self.mask_out_2 = read_images_from_disk(self.queue, self.input_size, self.surface, img_landmarks_data, img_bbox_data)

    def dequeue(self, num_elements):
        image_batch, mask_in_batch, mask_out_batch, mask_in_2_batch, mask_out_2_batch = tf.train.batch([self.image, self.mask_in, self.mask_out, self.mask_in_2, self.mask_out_2],
                                                 num_elements)
        return image_batch, mask_in_batch, mask_out_batch, mask_in_2_batch, mask_out_2_batch