import tensorflow as tf

def generator(x, istraining, reuse=tf.AUTO_REUSE):
    '''
    generator function
    arguments:
    - x: input image: a 96 x 96 x 3 tensor
    - istraining: regime (train/test) mainly for batch normalisation
    outputs:
    - image (scaled to 0-1)
    - image logits
    - embeddings (in-between fc layer)
    '''
    with tf.variable_scope('Generator', reuse=reuse):

        input_layer = x

        conv0_1 = tf.layers.conv2d(
           inputs=input_layer,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
        
        bn0_1= tf.contrib.layers.batch_norm(conv0_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv0_2 = tf.layers.conv2d(
           inputs=bn0_1,
           filters=64,
           kernel_size=[3, 3],
           strides=[2,2],
           padding="same",
           activation=None)
      
        bn0_2= tf.contrib.layers.batch_norm(conv0_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv1_1 = tf.layers.conv2d(
           inputs=bn0_2,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
      
        bn1_1= tf.contrib.layers.batch_norm(conv1_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv1_2 = tf.layers.conv2d(
           inputs=bn1_1,
           filters=64,
           kernel_size=[3, 3],
           strides=[2,2],
           padding="same",
           activation=None)
      
        bn1_2= tf.contrib.layers.batch_norm(conv1_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv2_1 = tf.layers.conv2d(
          inputs=bn1_2,
          filters=128,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
      
        bn2_1= tf.contrib.layers.batch_norm(conv2_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv2_2 = tf.layers.conv2d(
          inputs=bn2_1,
          filters=128,
          kernel_size=[3, 3],
          strides=[2,2],
          padding="same",
          activation=None)
      
        bn2_2= tf.contrib.layers.batch_norm(conv2_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv3_1 = tf.layers.conv2d(
          inputs=bn2_2,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn3_1= tf.contrib.layers.batch_norm(conv3_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv3_2 = tf.layers.conv2d(
          inputs=bn3_1,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn3_2= tf.contrib.layers.batch_norm(conv3_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv3_3 = tf.layers.conv2d(
          inputs=bn3_2,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn3_3= tf.contrib.layers.batch_norm(conv3_3, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv3_4 = tf.layers.conv2d(
          inputs=bn3_3,
          filters=256,
          kernel_size=[3, 3],
          strides=[2,2],
          padding="same",
          activation=None)
        
        bn3_4= tf.contrib.layers.batch_norm(conv3_4, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv4_1 = tf.layers.conv2d(
          inputs=bn3_4,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_1= tf.contrib.layers.batch_norm(conv4_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv4_2 = tf.layers.conv2d(
          inputs=bn4_1,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_2= tf.contrib.layers.batch_norm(conv4_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv4_3 = tf.layers.conv2d(
          inputs=bn4_2,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_3= tf.contrib.layers.batch_norm(conv4_3, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        conv4_4 = tf.layers.conv2d(
          inputs=bn4_3,
          filters=512,
          kernel_size=[3, 3],
          strides=[2,2],
          padding="same",
          activation=None)
        
        bn4_4= tf.contrib.layers.batch_norm(conv4_4, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        fc = tf.contrib.layers.flatten(bn4_4)
        fc = tf.layers.dense(fc, 4608)
        fc= tf.contrib.layers.batch_norm(fc, scale=True, is_training=istraining, activation_fn=tf.nn.relu)
        ds = tf.reshape(fc, shape=[-1, 3, 3, 512])
        
        ds= tf.concat([ds, bn4_4],3)
        
        deconv4_1 = tf.layers.conv2d_transpose(
          inputs=ds,
          filters=512,
          kernel_size=[3, 3],
          strides=2,
          padding="same",
          activation=None)
        
        debn4_1= tf.contrib.layers.batch_norm(deconv4_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv4_2 = tf.layers.conv2d(
          inputs=debn4_1,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn4_2= tf.contrib.layers.batch_norm(deconv4_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv4_3 = tf.layers.conv2d(
          inputs=debn4_2,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn4_3= tf.contrib.layers.batch_norm(deconv4_3, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv4_4 = tf.layers.conv2d(
          inputs=debn4_3,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn4_4= tf.contrib.layers.batch_norm(deconv4_4, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        debn4_4= tf.concat([debn4_4, bn3_4], 3)
        
        deconv3_1 = tf.layers.conv2d_transpose(
          inputs=debn4_4,
          filters=256,
          kernel_size=[3, 3],
          strides=2,
          padding="same",
          activation=None)
        
        debn3_1= tf.contrib.layers.batch_norm(deconv3_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv3_2 = tf.layers.conv2d(
          inputs=debn3_1,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn3_2= tf.contrib.layers.batch_norm(deconv3_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv3_3 = tf.layers.conv2d(
          inputs=debn3_2,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn3_3= tf.contrib.layers.batch_norm(deconv3_3, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv3_4 = tf.layers.conv2d(
          inputs=debn3_3,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn3_4= tf.contrib.layers.batch_norm(deconv3_4, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        debn3_4= tf.concat([debn3_4, bn2_2], 3)
        
        deconv2_1 = tf.layers.conv2d_transpose(
          inputs=debn3_4,
          filters=128,
          kernel_size=[3, 3],
          strides=2,
          padding="same",
          activation=None)
      
        debn2_1= tf.contrib.layers.batch_norm(deconv2_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv2_2 = tf.layers.conv2d(
          inputs=debn2_1,
          filters=128,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
      
        debn2_2= tf.contrib.layers.batch_norm(deconv2_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        debn2_2= tf.concat([debn2_2, bn1_2],3)

        deconv1_1 = tf.layers.conv2d_transpose(
           inputs=debn2_2,
           filters=64,
           kernel_size=[3, 3],
           strides=2,
           padding="same",
           activation=None)

        debn1_1= tf.contrib.layers.batch_norm(deconv1_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv1_2 = tf.layers.conv2d(
           inputs=debn1_1,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
      
        debn1_2= tf.contrib.layers.batch_norm(deconv1_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        debn1_2= tf.concat([debn1_2, bn0_2], 3)

        deconv0_1 = tf.layers.conv2d_transpose(
           inputs=debn1_2,
           filters=64,
           kernel_size=[3, 3],
           strides=2,
           padding="same",
           activation=None)

        debn0_1= tf.contrib.layers.batch_norm(deconv0_1, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv0_2 = tf.layers.conv2d(
           inputs=debn0_1,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
      
        debn0_2= tf.contrib.layers.batch_norm(deconv0_2, scale=True, is_training=istraining, activation_fn=tf.nn.relu)

        deconv0 = tf.layers.conv2d(debn0_2, 3, kernel_size=[1, 1],padding="same", activation=None)
        
    return tf.nn.sigmoid(deconv0), deconv0, fc
    
def maskrec(x, istraining, reuse=tf.AUTO_REUSE):
    '''
    mask reconstruction network for HnS loss
    arguments:
    - x: input image: a 96 x 96 x 3 tensor
    - istraining: regime (train/test) mainly for batch normalisation
    outputs:
    - reconstructed mask (scaled to 0-1)
    - reconstructed mask logits
    '''
    with tf.variable_scope('Maskrec', reuse=reuse):
        
        conv0_1 = tf.layers.conv2d(
           inputs=x,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
        
        bn0_1= tf.contrib.layers.batch_norm(conv0_1, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        conv0_2 = tf.layers.conv2d(
           inputs=bn0_1,
           filters=64,
           kernel_size=[3, 3],
           strides=[2,2],
           padding="same",
           activation=None)
      
        bn0_2= tf.contrib.layers.batch_norm(conv0_2, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        conv1_1 = tf.layers.conv2d(
           inputs=bn0_2,
           filters=128,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
      
        bn1_1= tf.contrib.layers.batch_norm(conv1_1, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)

        conv1_2 = tf.layers.conv2d(
           inputs=bn1_1,
           filters=128,
           kernel_size=[3, 3],
           strides=[2,2],
           padding="same",
           activation=None)
      
        bn1_2= tf.contrib.layers.batch_norm(conv1_2, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        conv2_1 = tf.layers.conv2d(
          inputs=bn1_2,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
      
        bn2_1= tf.contrib.layers.batch_norm(conv2_1, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        conv2_2 = tf.layers.conv2d(
          inputs=bn2_1,
          filters=256,
          kernel_size=[3, 3],
          strides=[2,2],
          padding="same",
          activation=None)
      
        bn2_2= tf.contrib.layers.batch_norm(conv2_2, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        y = tf.image.resize_images(images=bn2_2, size=[24, 24],method=tf.image.ResizeMethod.BILINEAR)
        deconv2_1 = tf.layers.conv2d(
          inputs=y,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn2_1= tf.contrib.layers.batch_norm(deconv2_1, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)

        y = tf.image.resize_images(images=debn2_1, size=[24, 24],method=tf.image.ResizeMethod.BILINEAR)
        deconv2_2 = tf.layers.conv2d(
          inputs=y,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn2_2= tf.contrib.layers.batch_norm(deconv2_2, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        ucat0= tf.concat([debn2_2, bn1_2], 3)
        
        deconv1_1 = tf.layers.conv2d(
          inputs=ucat0,
          filters=128,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn1_1= tf.contrib.layers.batch_norm(deconv1_1, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)

        y = tf.image.resize_images(images=debn1_1, size=[48, 48],method=tf.image.ResizeMethod.BILINEAR)
        deconv1_2 = tf.layers.conv2d(
          inputs=y,
          filters=128,
          kernel_size=[3, 3],
          padding="same",
          activation=None)

        debn1_2= tf.contrib.layers.batch_norm(deconv1_2, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        ucat1= tf.concat([debn1_2, bn0_2], 3)

        deconv0_1 = tf.layers.conv2d(
          inputs=ucat1,
          filters=64,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn0_1= tf.contrib.layers.batch_norm(deconv0_1, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)

        y = tf.image.resize_images(images=debn0_1, size=[96, 96],method=tf.image.ResizeMethod.BILINEAR)
        deconv0_2 = tf.layers.conv2d(
          inputs=y,
          filters=64,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        debn0_2= tf.contrib.layers.batch_norm(deconv0_2, scale=True, is_training=istraining,variables_collections=["batch_norm_non_trainable_variables_collection"], activation_fn=tf.nn.relu)
        
        conv0 = tf.layers.conv2d(
           inputs=debn0_2,
           filters=3,
           kernel_size=[1, 1],
           padding="same",
           activation=None)
    
        rec=tf.nn.sigmoid(conv0)
        
    return rec,conv0
   
def discriminator(x, istraining, reuse=tf.AUTO_REUSE):
    with tf.variable_scope('Discriminator', reuse=reuse):
        '''
        discriminator network for adversarial training
        arguments:
        - x: input image: a 96 x 96 x 3 tensor
        - istraining: regime (train/test) mainly for batch normalisation
        outputs:
        - binary classification (scaled to 0-1)
        - logit
        '''
    
        input_layer = x

        conv0_1 = tf.layers.conv2d(
           inputs=input_layer,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
      
        bn0_1= tf.contrib.layers.batch_norm(conv0_1, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv0_2 = tf.layers.conv2d(
           inputs=bn0_1,
           filters=64,
           kernel_size=[3, 3],
           strides=[2,2],
           padding="same",
           activation=None)
      
        bn0_2= tf.contrib.layers.batch_norm(conv0_2, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv1_1 = tf.layers.conv2d(
           inputs=bn0_2,
           filters=64,
           kernel_size=[3, 3],
           padding="same",
           activation=None)
      
        bn1_1= tf.contrib.layers.batch_norm(conv1_1, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv1_2 = tf.layers.conv2d(
           inputs=bn1_1,
           filters=64,
           kernel_size=[3, 3],
           strides=[2,2],
           padding="same",
           activation=None)
      
        bn1_2= tf.contrib.layers.batch_norm(conv1_2, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv2_1 = tf.layers.conv2d(
          inputs=bn1_2,
          filters=128,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
      
        bn2_1= tf.contrib.layers.batch_norm(conv2_1, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv2_2 = tf.layers.conv2d(
          inputs=bn2_1,
          filters=128,
          kernel_size=[3, 3],
          strides=[2,2],
          padding="same",
          activation=None)
      
        bn2_2= tf.contrib.layers.batch_norm(conv2_2, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv3_1 = tf.layers.conv2d(
          inputs=bn2_2,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn3_1= tf.contrib.layers.batch_norm(conv3_1, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv3_2 = tf.layers.conv2d(
          inputs=bn3_1,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn3_2= tf.contrib.layers.batch_norm(conv3_2, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv3_3 = tf.layers.conv2d(
          inputs=bn3_2,
          filters=256,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn3_3= tf.contrib.layers.batch_norm(conv3_3, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv3_4 = tf.layers.conv2d(
          inputs=bn3_3,
          filters=256,
          kernel_size=[3, 3],
          strides=[2,2],
          padding="same",
          activation=None)
        
        bn3_4= tf.contrib.layers.batch_norm(conv3_4, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv4_1 = tf.layers.conv2d(
          inputs=bn3_4,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_1= tf.contrib.layers.batch_norm(conv4_1, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv4_2 = tf.layers.conv2d(
          inputs=bn4_1,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_2= tf.contrib.layers.batch_norm(conv4_2, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv4_3 = tf.layers.conv2d(
          inputs=bn4_2,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_3= tf.contrib.layers.batch_norm(conv4_3, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        conv4_4 = tf.layers.conv2d(
          inputs=bn4_3,
          filters=512,
          kernel_size=[3, 3],
          padding="same",
          activation=None)
        
        bn4_4= tf.contrib.layers.batch_norm(conv4_4, scale=True, is_training=istraining, activation_fn=tf.nn.leaky_relu)

        out = tf.contrib.layers.flatten(bn4_4)
        out_0 = tf.layers.dense(out, 1)
    return tf.nn.sigmoid(out_0), out_0